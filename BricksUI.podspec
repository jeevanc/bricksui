Pod::Spec.new do |spec|
  spec.name                = "BricksUI"
  spec.version             = "0.1.1"
  spec.summary             = "The Design System UI."
  spec.homepage            = "https://ds.jio.com/"
  spec.license             = { :type => "MIT" }
  spec.author              = { "Bricks" => "?" }
  spec.platform            = :ios, "13.0"
  spec.swift_versions      = ["5.0"]
  spec.frameworks          = "SwiftUI"
  spec.source             = { :git => "https://gitlab.com/jeevanc/bricksui", :tag => "#{spec.version}" }
  spec.source_files        = "BricksUI/**/*.swift"
  spec.resource_bundle     = { "Assets" => "BricksUI/**/*.xcassets" }
end
